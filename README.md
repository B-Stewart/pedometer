# README #

This is a pedometer example application using a Android Library (.aar) to run all of the service calls and logic. The algorithm and service code was obtained at https://github.com/phishman3579/android-pedometer and is license under Apache 2.0

### What is this repository for? ###

* This provides an easy .aar library for access in multiple apps. Specifically we will be using it as a Unity plugin.


### Licensing Information ###
* This repository is a modification of https://github.com/phishman3579/android-pedometer written by Justin Wetherell.
* The original code was released under Apache License 2.0 as is this project. 
* I cannot be held liable for any misuse or modification of this code base including any damages it may cause. 