// IStepServiceCallback.aidl
package com.swiftsoulinteractive.pedometerlib.service;


interface IStepServiceCallback {
    void stepsChanged(int value);
}
