// IStepService.aidl
package com.swiftsoulinteractive.pedometerlib.service;

import com.swiftsoulinteractive.pedometerlib.service.IStepServiceCallback;

interface IStepService {
		boolean isRunning();
		void setSensitivity(int sens);
		void registerCallback(IStepServiceCallback cb);
		void unregisterCallback(IStepServiceCallback cb);
}
