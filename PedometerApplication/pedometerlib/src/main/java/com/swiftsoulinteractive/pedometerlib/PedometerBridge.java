package com.swiftsoulinteractive.pedometerlib;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;

import com.unity3d.player.UnityPlayer;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Brayden on 1/22/2016.
 */
public class PedometerBridge {

    Pedometer pedometer;

    public PedometerBridge()
    {

    }

    public void Init(Activity activity)
    {
        pedometer = new Pedometer(activity);
    }

    public void InitUnity()
    {
        final AtomicBoolean done = new AtomicBoolean(false);
        UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pedometer = new Pedometer(UnityPlayer.currentActivity);
                done.set(true);
            }
        });
        while (!done.get())
        {}

    }

    public int GetCurrentSteps()
    {
        return pedometer.GetSteps();
    }

    public void Start()
    {
        pedometer.StartPedometer();
    }

    public void Stop()
    {
        pedometer.StopPedometer();
    }

    public void ResetSteps()
    {
        pedometer.ResetSteps();
    }

    public boolean IsRunning()
    {
        return pedometer.IsPedometerRunning();
    }

    public void SetEventHandle(Handler aHandler)
    {
        pedometer.SetEventHandle(aHandler);
    }

    public Handler GetEventHandle()
    {
        return pedometer.GetEventHandle();
    }

    /**
     * Lower values make it more sensitive.
     * Negative values don't work
     * 100-150 is average for accuracy.
     * @param sensitivity
     *
     */
    public void SetSensitivity(final int sensitivity)
    {
        pedometer.SetSensitivity(sensitivity);
    }

    public int GetSensitivity()
    {
        return pedometer.GetSensitivity();
    }

    public static String TestReturnString()
    {
        return "Static String";
    }


}
