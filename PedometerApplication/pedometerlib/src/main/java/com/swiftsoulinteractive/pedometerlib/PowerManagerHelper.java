package com.swiftsoulinteractive.pedometerlib;

import android.content.Context;
import android.os.PowerManager;

/**
 * Created by Brayden on 1/22/2016.
 */
public class PowerManagerHelper
{
    private PowerManager powerManager = null;
    private PowerManager.WakeLock wakeLock = null;

    public PowerManagerHelper(Context mContext)
    {
        powerManager = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "PedometerLock");
    }

    public void AquireWakeLock()
    {
        if(!wakeLock.isHeld())
            wakeLock.acquire();
    }

    public void ReleaseWakeLock()
    {
        if(wakeLock.isHeld())
            wakeLock.release();
    }

}
