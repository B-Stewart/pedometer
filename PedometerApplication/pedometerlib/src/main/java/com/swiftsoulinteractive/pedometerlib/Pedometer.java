package com.swiftsoulinteractive.pedometerlib;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;

import com.swiftsoulinteractive.pedometerlib.service.IStepService;
import com.swiftsoulinteractive.pedometerlib.service.IStepServiceCallback;
import com.swiftsoulinteractive.pedometerlib.service.StepService;

import java.util.logging.Logger;

/**
 * Created by Brayden on 1/22/2016.
 */
public class Pedometer {
    private static boolean isRunning = false;
    private static int steps = 0;
    private static final Logger logger = Logger.getLogger(PedometerBridge.class.getSimpleName());
    private static Intent stepServiceIntent = null;
    private static PowerManagerHelper powerManager;
    private static IStepService mService = null;
    private static Context mApplicationContext;
    private static int sensitivity = 100;
    private static Handler eventHandle;

    Pedometer(Activity activity) {
        mApplicationContext = activity.getApplicationContext();
        powerManager = new PowerManagerHelper(mApplicationContext);
        if (stepServiceIntent == null) {
            Bundle extras = new Bundle();
            extras.putInt("int", 1);
            stepServiceIntent = new Intent(mApplicationContext, StepService.class);
            stepServiceIntent.putExtras(extras);
        }

        mApplicationContext.bindService(stepServiceIntent, mConnection, 0);
    }

    public void SetEventHandle(Handler aHandler)
    {
        eventHandle = aHandler;
    }

    public Handler GetEventHandle()
    {
        return eventHandle;
    }

    public void StartPedometer() {
        powerManager.AquireWakeLock();
        start();
        isRunning = true;
    }

    public void StopPedometer() {
        powerManager.ReleaseWakeLock();
        stop();
        isRunning = false;
    }

    public void ResetSteps() {
        steps = 0;
    }

    public boolean IsPedometerRunning() {
        return isRunning;
    }

    public int GetSteps() {
        return steps;
    }

    public void SetSensitivity(int sensitive)
    {
        if(sensitive<0)
            sensitive = 0;
        sensitivity = sensitive;
    }

    public int GetSensitivity()
    {
        return sensitivity;
    }

    private void start() {

        startStepService();
        bindStepService();
    }

    private void stop() {

        unbindStepService();
        stopStepService();
    }

    private void startStepService() {
        try {
            mApplicationContext.startService(stepServiceIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopStepService() {
        try {
            mApplicationContext.stopService(stepServiceIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean bindStepService() {
        try {
            boolean created = mApplicationContext.bindService(stepServiceIntent, mConnection, Context.BIND_AUTO_CREATE);
            if(!created)
                return false;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return  true;
    }

    private void unbindStepService() {
        try {
            mApplicationContext.unbindService(mConnection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            if(mService == null)
            {
                mService = IStepService.Stub.asInterface(service);
                try {
                    mService.registerCallback(mCallback);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            //Always reset sensitivity
            try {
                mService.setSensitivity(sensitivity);
            } catch (RemoteException e) {
                e.printStackTrace();
            }

        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            /*
            try {
                //startStopButton.setChecked(mService.isRunning());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            */
            mService = null;
        }
    };

    private static final IStepServiceCallback.Stub mCallback = new IStepServiceCallback.Stub() {

        @Override
        public IBinder asBinder() {
            return mCallback;
        }

        @Override
        public void stepsChanged(int value) throws RemoteException {
            Message msg = handler.obtainMessage();
            msg.arg1 = value;
            handler.sendMessage(msg);
            if(eventHandle != null)
                eventHandle.sendMessage(msg);
        }
    };

    private static final Handler handler = new Handler() {

        public void handleMessage(Message msg) {
            int current = msg.arg1;
            logger.info("stepsChanged() " + current);
            steps += current;
        }
    };

}
